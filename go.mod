module echologrus

go 1.13

require (
	github.com/labstack/gommon v0.3.0
	github.com/mailgun/logrus-hooks v1.4.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
)
