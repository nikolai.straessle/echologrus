package echologrus

import (
	"bytes"
	"github.com/labstack/gommon/log"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"io"
	"reflect"
	"testing"
)

var testLogger, _ = GetEchoLogger("localhost", 50)

func TestLogrus_Debug(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		i []interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "TestLogrus_Debug",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args: args{
				i: []interface{}{"test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.Debug(tt.args.i)
		})
	}
}

func TestLogrus_Debugj(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		j log.JSON
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "TestLogrus_Debugj",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args: args{
				j: map[string]interface{}{"message": "test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.Debugj(tt.args.j)
		})
	}
}

func TestLogrus_Error(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		i []interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Error",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				i: []interface{}{"test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.Error(tt.args.i)
		})
	}
}

func TestLogrus_Errorj(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		j log.JSON
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Errorj",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				j: map[string]interface{}{"message": "test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.Errorj(tt.args.j)
		})
	}
}

func TestLogrus_Fatal(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		i []interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Fatal",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				i: []interface{}{"test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.ExitFunc = func(i int) {
				if 1 != i {
					t.Errorf("fail to log fatal")
				}
			}
			l.Fatal(tt.args.i)
		})
	}
}

func TestLogrus_Fatalj(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		j log.JSON
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Fatalj",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				j: map[string]interface{}{"message": "test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.ExitFunc = func(i int) {
				if 1 != i {
					t.Errorf("fail to log fatal")
				}
			}
			l.Fatalj(tt.args.j)
		})
	}
}

func TestLogrus_Info(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		i []interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Info",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				i: []interface{}{"test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.Info(tt.args.i)
		})
	}
}

func TestLogrus_Infoj(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		j log.JSON
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Infoj",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				j: map[string]interface{}{"message": "test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.Infoj(tt.args.j)
		})
	}
}

func TestLogrus_Level(t *testing.T) {
	testLogger.SetLevel(log.INFO)
	debugLogger,_ := GetEchoLogger("", 0)
	debugLogger.SetLevel(log.DEBUG)
	warnLogger,_ := GetEchoLogger("", 0)
	warnLogger.SetLevel(log.WARN)
	errorLogger,_ := GetEchoLogger("", 0)
	errorLogger.SetLevel(log.ERROR)
	offLogger,_ := GetEchoLogger("", 0)
	offLogger.SetLevel(99)

	type fields struct {
		Logger *logrus.Logger
	}
	tests := []struct {
		name   string
		fields fields
		want   log.Lvl
	}{
		{
			name:   "TestLogrus_LevelInfo",
			fields: fields{
				Logger: testLogger.Logger,
			},
			want:   2,
		},
		{
			name:   "TestLogrus_LevelDebug",
			fields: fields{
				Logger: debugLogger.Logger,
			},
			want:   1,
		},
		{
			name:   "TestLogrus_LevelWarn",
			fields: fields{
				Logger: warnLogger.Logger,
			},
			want:   3,
		},
		{
			name:   "TestLogrus_LevelError",
			fields: fields{
				Logger: errorLogger.Logger,
			},
			want:   4,
		},
		{
			name:   "TestLogrus_LevelOff",
			fields: fields{
				Logger: offLogger.Logger,
			},
			want:   5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			if got := l.Level(); got != tt.want {
				t.Errorf("Level() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLogrus_Output(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	tests := []struct {
		name   string
		fields fields
		want   io.Writer
	}{
		{
			name:   "TestLogrus_Output",
			fields: fields{
				Logger: testLogger.Logger,
			},
			want:   nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			if got := l.Output(); reflect.DeepEqual(got, nil) {
				t.Errorf("Output() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLogrus_Panic(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		i []interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Panic",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				i: []interface{}{"test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			assert.Panics(t, func() {
				l.Panic(tt.args.i)
			}, "no panic")
		})
	}
}

func TestLogrus_Panicj(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		j log.JSON
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Panicj",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				j: map[string]interface{}{"message": "test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			assert.Panics(t, func() {
				l.Panicj(tt.args.j)
			}, "no panic")
		})
	}
}

func TestLogrus_Prefix(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name:   "TestLogrus_Prefix",
			fields: fields{
				Logger: testLogger.Logger,
			},
			want:   "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			if got := l.Prefix(); got != tt.want {
				t.Errorf("Prefix() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLogrus_Print(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		i []interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Print",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				i: []interface{}{"test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.Print(tt.args.i)
		})
	}
}

func TestLogrus_Printj(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		j log.JSON
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Printj",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				j: map[string]interface{}{"message": "test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.Printj(tt.args.j)
		})
	}
}

func TestLogrus_SetHeader(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		in0 string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_SetHeader",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				in0: "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.SetHeader(tt.args.in0)
		})
	}
}

func TestLogrus_SetOutput(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	tests := []struct {
		name   string
		fields fields
		wantW  string
	}{
		{
			name:   "TestLogrus_SetOutput",
			fields: fields{
				Logger: testLogger.Logger,
			},
			wantW:  "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			w := &bytes.Buffer{}
			l.SetOutput(w)
			if gotW := w.String(); gotW != tt.wantW {
				t.Errorf("SetOutput() = %v, want %v", gotW, tt.wantW)
			}
		})
	}
}

func TestLogrus_SetPrefix(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		s string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_SetPrefix",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				s: "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.SetPrefix(tt.args.s)
		})
	}
}

func TestLogrus_Warn(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		i []interface{}
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Warn",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				i: []interface{}{"test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.Warn(tt.args.i)
		})
	}
}

func TestLogrus_Warnj(t *testing.T) {
	type fields struct {
		Logger *logrus.Logger
	}
	type args struct {
		j log.JSON
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name:   "TestLogrus_Warnj",
			fields: fields{
				Logger: testLogger.Logger,
			},
			args:   args{
				j: map[string]interface{}{"message": "test msg"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := Logrus{
				Logger: tt.fields.Logger,
			}
			l.Warnj(tt.args.j)
		})
	}
}

func TestGetEchoLogger(t *testing.T) {
	type args struct {
		host string
		port int
	}
	tests := []struct {
		name    string
		args    args
		want    Logrus
		wantErr bool
	}{
		{
			name: "TestGetEchoLogger",
			args: args{
				host: "localhost",
				port: 60,
			},
			want:    Logrus{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := GetEchoLogger(tt.args.host, tt.args.port)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetEchoLogger() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
