#!/usr/bin/env bash
go test -v ./... 2>&1 | go-junit-report > report.xml
go test -coverprofile=c.out ./...
go tool cover -html=c.out -o coverage.html
rm report.xml